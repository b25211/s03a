create table users (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(55) NOT NULL,
    password varchar(55) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
);

create table posts (
    id int NOT NULL AUTO_INCREMENT,
    title varchar(55) NOT NULL,
    content varchar(255) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
);

INSERT INTO users (email, password, datetime_created) 
    VALUES  ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
            ("juandelazruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
            ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
            ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
            ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

INSERT INTO posts (title, content, datetime_posted) 
    VALUES  ("Frist Code", "Hello World!", "2021-01-01 01:00:00"),
            ("Second Code", "Hello Earth!", "2021-01-01 02:00:00"),
            ("Third Code", "Welcome to Mars!", "2021-01-01 03:00:00"),
            ("Fourth Code", "Bye bye solar System!", "2021-01-01 04:00:00");

SELECT * FROM posts where id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 1;

DELETE FROM users WHERE email = "OPM" AND length > 240;
