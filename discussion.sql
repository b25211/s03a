INSERT INTO artists (name) VALUES ('Rivermaya');
INSERT INTO artists (name) VALUES ('Psy');

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy-6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253 ,"K-Pop", 3); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234 ,"OPM", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmate", 279 ,"OPM", 4);

-- Retrieve the title and genre of all ALL songs
SELECT song_name, genre FROM songs;


--  REtrieve all records from the songs table
SELECT * FROM songs;

-- Retrieve records with a condition
SELECT song_name FROM songs WHERE genre  = "OPM";

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- [Section] Updating an existing record from a tables
-- This query will update the 'Kundiman' song's length tp 240
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- [Section] Deleting an existing record from a tables
DELETE FROM songs WHERE genre = "OPM" AND length > 240;